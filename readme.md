# Markiza Sluzobnik
Markíza Služobník od slova sluha je aplikácia ktorá vytvára API "sluhu" pre aplikácie Markíza.sk Tvnoviny.sk Voyo.sk. Súčasťou aplikácie sú public a private API

# 1. Instalacia

## Vyklonovanie kodu **s nastavenim VirtualHostu** - ODPORUCANE
* v adresari **C/xampp/htdocs** vyklonujte kod do foldru markiza-sluzobnik
  ```
  git clone https://surname.name:password@bit.dev.markiza.sk/scm/ser/markiza-sluzobnik.git ./markiza-sluzobnik
  ```
* v adresari **C:/xampp/apache/conf/extra/httpd-vhosts.conf** si nakonfigurujeme novy VirtualHost
  ```
  <VirtualHost *:80>
   DocumentRoot "c:/xampp/htdocs/markiza-sluzobnik/www"
   ServerName markizaservant.loc
   <Directory "c:/xampp/htdocs/markiza-sluzobnik/www">
   </Directory>
  </VirtualHost>
  ```
* v adresari **C:/Windows/System32/drivers/etc/hosts** pridame domenu **markizaservant.loc** tak aby smerovala na localhost - **127.0.0.1**
  ```
  127.0.0.1       markizaservant.loc
  ```
* restartovat **Apache** 
* Ak je vsetko v poriadku aplikacia sa da otvorit na http://markizaforms.loc cez lubovolny prehliadac
* 

### Docker Compose

```bash
git clone https://surname.name:password@bit.dev.markiza.sk/scm/ser/markiza-sluzobnik.git ./markiza-sluzobnik
cd markiza-sluzobnik
docker-compose up
```
otvor adresu z konzoly, pravdepodobne **http://172.18.0.2** alebo **http://localhost:8001**

### Docker Pull - zatial nie je pushnute na docker hosting

```bash
docker pull digilopment/markiza-sluzobnik:latest
docker run digilopment/markiza-sluzobnik
```
otvor adresu z konzoly, pravdepodobne **http://172.18.0.2**

# 2. Features
### Code Checker
```bash
 vendor/bin/phpstan analyse app
```

### Doctrine/Nettrine generate entities
```bash
 cd  /c/xampp/htdocs/markiza-sluzobnik
 bin/console orm:convert-mapping --namespace="App\\Entity\\" --force  --from-database annotation ./
```

# 3. Dostupné moduly a ich služby

## 1. API

**Fortina liga** - služba pre nastavenie rozpätia hracích časov v rámci Fortuna Ligy
    ```
    {HOST}/api/v1/fortuna-liga
    ```
    
**Covid World** - plugin pre ziskanie dat z www.worldometers.info . Služba CovidWorld data su cachovane v presenteri.
    ```
    {HOST}/api/v1/covid-world
    ```

## 2. SERVICES

**No Service** 
    ```
    {HOST}/service/{serviceName}
    ```
    
## 3. CRON/JOBS

**No Job** 
    ```
    {HOST}/v1/job/{jobName}
    ```