<?php declare(strict_types = 1);

namespace App\GlobalServices\Database;

use Nettrine\ORM\EntityManagerDecorator as NettrineEntityManagerDecorator;

/**
 * Custom EntityManagerDecorator
 */
final class EntityManagerDecorator extends NettrineEntityManagerDecorator
{

	use TRepositories;

}
