<?php

namespace App\GlobalServices\Router;

class ApiRouter
{

    public function routes()
    {
        return array(
            'api/' => 'Home:default',
            'api/v1/' => 'Home:default',
            'api/v1/fortuna-liga' => 'FortunaLiga:default',
            'api/v1/covid-world' => 'CovidWorld:default',
            'api/v1/database' => 'Database:default',
            'api/v1/<id>' => 'Api:default',
        );
    }

}
