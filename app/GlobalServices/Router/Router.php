<?php

namespace App\GlobalServices;

use Nette\Application\Routers\RouteList;

final class Router
{

    private static function loadRouter(string $class)
    {
        $clsName = 'App\GlobalServices\Router\\' . $class;
        $router = new $clsName;
        return $router->routes();
    }

    private static function useRouter()
    {
        $router['api'] = self::loadRouter('ApiRouter');
        $router['default'] = self::loadRouter('DefaultRouter');
        return $router;
    }

    public static function createRouter(): RouteList
    {
        $router = new RouteList;

        $routes = self::useRouter();
        foreach (array_keys($routes) as $routeType) {
            foreach ($routes[$routeType] as $route => $presenter) {
                $router->addRoute($route, $presenter);
            }
        }
        return $router;
    }

}
