<?php

namespace App\GlobalServices;

class Std
{

    public function name_url($str)
    {
        $this->str = preg_replace('/[^\pL0-9_]+/u', '-', $str);
        $this->str = trim($this->str, '-');
        $this->str = iconv('utf-8', 'ASCII//TRANSLIT', $this->str);
        $this->str = strtolower($this->str);
        $this->str = preg_replace('/[^-a-z0-9_]+/', '', $this->str);
        return $this->str;
    }

}
