<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Settings
 *
 * @ORM\Table(name="settings")
 * @ORM\Entity
 */
class Settings
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="key", type="string", length=300, nullable=false)
     */
    private $key;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="text", length=65535, nullable=false)
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=1000, nullable=false)
     */
    private $description;

    function getId(): int
    {
        return $this->id;
    }

    function getKey(): string
    {
        return $this->key;
    }

    function getValue(): string
    {
        return $this->value;
    }

    function getDescription(): string
    {
        return $this->description;
    }

    function setId(int $id): void
    {
        $this->id = $id;
    }

    function setKey(string $key): void
    {
        $this->key = $key;
    }

    function setValue(string $value): void
    {
        $this->value = $value;
    }

    function setDescription(string $description): void
    {
        $this->description = $description;
    }

}
