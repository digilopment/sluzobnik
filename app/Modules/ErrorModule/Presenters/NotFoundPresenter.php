<?php

namespace App\Presenters;

use Nette\Application\UI\Presenter;

final class NotFoundPresenter extends Presenter
{

    public $json;

    public function startup(): void
    {
        parent::startup();
        $data = [
            'message' => 'Page not found'
        ];
        $this->json = $data;
    }

    public function actionDefault(): void
    {
        $this->sendJson($this->json);
    }

}
