<?php

namespace App\Presenters;

use App\Modules\Api\Services\CovidWorld;
use Nette\Application\UI\Presenter;
use Nette\Caching\Cache;
use Nette\Caching\Storage;

final class CovidWorldPresenter extends Presenter
{

    public $covidWorld;
    public $cache;
    public $storage;

    const CACHE_ID = 'covidWorldData';
    const CACHE_EXPIRY = '15 minutes';

    public function __construct(CovidWorld $covidWorld, Cache $cache, Storage $storage)
    {
        parent::__construct();
        $this->covidWorld = $covidWorld;
        $this->cache = $cache;
        $this->storage = $storage;
    }

    protected function data()
    {
        $this->covidWorld->init();
        $data = $this->covidWorld->finalData;
        return $data;
    }

    protected function cache($key, $expire)
    {
        $cache = new Cache($this->storage, $key);
        $value = $cache->load($key, function (&$dependencies) use($expire) {
            $dependencies[Cache::EXPIRE] = $expire;
            $data = $this->data();
            return $data;
        });
        return $value;
    }

    public function actionDefault(): void
    {
        $data = $this->cache(self::CACHE_ID, self::CACHE_EXPIRY);
        $this->sendJson($data);
    }

}
