<?php

namespace App\Presenters;

use App\Modules\Api\Services\FortunaLiga;
use Nette\Application\UI\Presenter;

final class FortunaLigaPresenter extends Presenter
{

    /** @inject */
    public FortunaLiga $fortunaLiga;

    public function __construct(FortunaLiga $fortunaLiga)
    {
        parent::__construct();
        $this->fortunaLiga = $fortunaLiga;
    }

    public function actionDefault(): void
    {
        $this->fortunaLiga->init();
        $this->sendJson($this->fortunaLiga->response);
    }

}
