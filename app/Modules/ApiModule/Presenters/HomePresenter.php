<?php

namespace App\Presenters;

use Nette\Application\UI\Presenter;

final class HomePresenter extends Presenter
{

    public $json;

    public function startup(): void
    {
        parent::startup();
        $data = [
            'message' => 'Welcome to Markiza Api'
        ];
        $this->json = $data;
    }

    public function actionDefault(): void
    {
        $this->sendJson($this->json);
    }

}
