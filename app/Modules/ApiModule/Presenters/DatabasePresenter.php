<?php

namespace App\Presenters;

use Nettrine\ORM\EntityManagerDecorator;
use Nette\Application\UI\Presenter;

final class DatabasePresenter extends Presenter
{

    /**
     * @var $em \Doctrine\ORM\EntityManagerDecorator 
     */
    private $em;

    public function __construct(EntityManagerDecorator $em)
    {
        parent::__construct();
        $this->em = $em;
    }

    public function sampleDataRepository()
    {
        $criteria = [];
        $orderBy = ['id' => 'DESC'];
        return $this->em->getRepository('\App\Entity\Settings')->findBy($criteria, $orderBy);
    }

    protected function sampleData()
    {
        $builder = $this->em->createQueryBuilder()->select('u')->from('\App\Entity\Settings', 'u');
        $results = $builder->getQuery()->getResult();
        return $results;
    }

    public function actionDefault(): void
    {
        $final = [];
        foreach ($this->sampleDataRepository() as $item) {
            $final['id'] = $item->getId();
            $final['key'] = $item->getKey();
            $final['description'] = $item->getDescription();
        }
        $this->sendJson($final);
    }

}
