<?php

namespace App\Modules\Api\Services;

use Nette\Utils\DateTime;

class FortunaLiga extends FortunaLigaConfig
{

    public $response;

    protected function today()
    {
        return date('Y') . '-' . date('m') . '-' . date('d');
    }

    public function compareFixtures()
    {

        $arr = $this->fixtures;
        $today = $this->today();
        $final['start'] = $today;
        $final['stop'] = $today;
        $final['expiry'] = $today;
        $final['round'] = 1;

        foreach ($arr as $key => $fixture) {
            $dateStart = key($fixture);
            $dateStop = $fixture[$dateStart];

            $d1 = new DateTime($today);
            $d2 = new DateTime($dateStart);
            $d3 = new DateTime($dateStop);

            $expiry = date_create($dateStop);
            date_add($expiry, date_interval_create_from_date_string('2 days'));
            $dateExpiry = date_format($expiry, 'Y-m-d');

            $fromToday = date_create($today);
            date_add($fromToday, date_interval_create_from_date_string('30 days'));
            $todayPlusThirty = date_format($fromToday, 'Y-m-d');

            if ($d2 >= $d1 || $d1 <= $d3) {
                $final['start'] = $dateStart;
                $final['stop'] = $dateStop;
                $final['expiry'] = $dateExpiry;
                $final['round'] = $key;
                $final['todayPlusThirty'] = $todayPlusThirty;
                $final['today'] = $today;
                $final['tvDajto'] = $this->tvDajto;
                $final['tvNovaSport'] = $this->tvNovaSport;
                return $final;
            }
        }
        return $final;
    }

    public function init()
    {
        $data = $this->configurator();
        $this->fixtures = $data['rounds'];
        $this->tvDajto = $data['tv-dajto'];
        $this->tvNovaSport = $data['tv-nova-sport'];
        $response = $this->compareFixtures();
        $this->response = $response;
    }

}
