<?php

declare(strict_types=1);

namespace PackageVersions;

use Composer\InstalledVersions;
use OutOfBoundsException;

class_exists(InstalledVersions::class);

/**
 * This class is generated by composer/package-versions-deprecated, specifically by
 * @see \PackageVersions\Installer
 *
 * This file is overwritten at every run of `composer install` or `composer update`.
 *
 * @deprecated in favor of the Composer\InstalledVersions class provided by Composer 2. Require composer-runtime-api:^2 to ensure it is present.
 */
final class Versions
{
    /**
     * @deprecated please use {@see self::rootPackageName()} instead.
     *             This constant will be removed in version 2.0.0.
     */
    const ROOT_PACKAGE_NAME = 'nette/web-project';

    /**
     * Array of all available composer packages.
     * Dont read this array from your calling code, but use the \PackageVersions\Versions::getVersion() method instead.
     *
     * @var array<string, string>
     * @internal
     */
    const VERSIONS          = array (
  'composer/package-versions-deprecated' => '1.11.99.4@b174585d1fe49ceed21928a945138948cb394600',
  'contributte/console' => 'v0.9.1@549893573ba3cb81f476785763f48178b5166322',
  'contributte/di' => 'v0.5.1@534fdb5e85b4ae01f8f848fc4b752deb8458ed7c',
  'doctrine/annotations' => '1.13.2@5b668aef16090008790395c02c893b1ba13f7e08',
  'doctrine/cache' => '1.12.1@4cf401d14df219fa6f38b671f5493449151c9ad8',
  'doctrine/collections' => '1.6.8@1958a744696c6bb3bb0d28db2611dc11610e78af',
  'doctrine/common' => '3.2.0@6d970a11479275300b5144e9373ce5feacfa9b91',
  'doctrine/data-fixtures' => '1.5.1@f18adf13f6c81c67a88360dca359ad474523f8e3',
  'doctrine/dbal' => '2.13.4@2411a55a2a628e6d8dd598388ab13474802c7b6e',
  'doctrine/deprecations' => 'v0.5.3@9504165960a1f83cc1480e2be1dd0a0478561314',
  'doctrine/event-manager' => '1.1.1@41370af6a30faa9dc0368c4a6814d596e81aba7f',
  'doctrine/inflector' => '2.0.4@8b7ff3e4b7de6b2c84da85637b59fd2880ecaa89',
  'doctrine/instantiator' => '1.4.0@d56bf6102915de5702778fe20f2de3b2fe570b5b',
  'doctrine/lexer' => '1.2.1@e864bbf5904cb8f5bb334f99209b48018522f042',
  'doctrine/migrations' => '2.3.4@6d87c9a0baa6a4725b4c4e1a45b2a39f53bf1859',
  'doctrine/orm' => '2.10.2@81d472f6f96b8b571cafefe8d2fef89ed9446a62',
  'doctrine/persistence' => '2.2.3@5e7bdbbfe9811c06e1f745d1c166647d5c47d6ee',
  'friendsofphp/proxy-manager-lts' => 'v1.0.5@006aa5d32f887a4db4353b13b5b5095613e0611f',
  'laminas/laminas-code' => '4.4.3@bb324850d09dd437b6acb142c13e64fdc725b0e1',
  'latte/latte' => 'v2.10.5@0138a26efe7a2a448769f6ba490bfaae8fe7ab50',
  'nette/application' => 'v3.1.4@db6eedc199732ed6cf9982da97a38219e8601c62',
  'nette/bootstrap' => 'v3.1.1@efe6c30fc009451f59fe56f3b309eb85c48b2baf',
  'nette/caching' => 'v3.1.1@3e771c589dee414724be473c24ad16dae50c1960',
  'nette/component-model' => 'v3.0.2@20a39df12009029c7e425bc5e0439ee4ab5304af',
  'nette/database' => 'v3.1.3@08aff4d62c80873752254b0b655027211f501c4d',
  'nette/di' => 'v3.0.11@942e406f63b88b57cb4e095ae0fd95c103d12c5b',
  'nette/finder' => 'v2.5.2@4ad2c298eb8c687dd0e74ae84206a4186eeaed50',
  'nette/forms' => 'v3.1.6@4ed52434b61d7e532cb3bc77b048717703b91b0b',
  'nette/http' => 'v3.1.3@999f54884deed7a419b13552271c7059a5acb428',
  'nette/mail' => 'v3.1.7@0825a071b896c2ed8cbe1af776c2539acd7bb976',
  'nette/neon' => 'v3.3.1@1f4e5f6a30bf45b6c2c932be7396ea70692ee607',
  'nette/php-generator' => 'v3.6.4@b8375ac20760c62b6816f8c2eaeabbbca305eed7',
  'nette/robot-loader' => 'v3.4.1@e2adc334cb958164c050f485d99c44c430f51fe2',
  'nette/routing' => 'v3.0.2@5532e7e3612e13def357f089c1a5c25793a16843',
  'nette/schema' => 'v1.2.2@9a39cef03a5b34c7de64f551538cbba05c2be5df',
  'nette/security' => 'v3.1.5@c120893f561b09494486c66594720b2abcb099b2',
  'nette/utils' => 'v3.2.5@9cd80396ca58d7969ab44fc7afcf03624dfa526e',
  'nettrine/annotations' => 'v0.7.0@fbb06d156a4edcbf37e4154e5b4ede079136388b',
  'nettrine/cache' => 'v0.3.0@8a58596de24cdd61e45866ef8f35788675f6d2bc',
  'nettrine/dbal' => 'v0.7.0@26a63ef21d229aa35b42b2d7798581bdb88ee912',
  'nettrine/fixtures' => 'v0.6.0@395476f575f48cd37d66f7856b0804b9534455f8',
  'nettrine/migrations' => 'v0.7.1@71951f68fec491e484e628047a3a2fe21469a518',
  'nettrine/orm' => 'v0.8.2@fc84bccce8665255519266351bb6b746d1d58650',
  'psr/cache' => '3.0.0@aa5030cfa5405eccfdcb1083ce040c2cb8d253bf',
  'psr/container' => '1.1.2@513e0666f7216c7459170d56df27dfcefe1689ea',
  'symfony/console' => 'v5.3.10@d4e409d9fbcfbf71af0e5a940abb7b0b4bad0bd3',
  'symfony/deprecation-contracts' => 'v2.4.0@5f38c8804a9e97d23e0c8d63341088cd8a22d627',
  'symfony/filesystem' => 'v5.3.4@343f4fe324383ca46792cae728a3b6e2f708fb32',
  'symfony/polyfill-ctype' => 'v1.23.0@46cd95797e9df938fdd2b03693b5fca5e64b01ce',
  'symfony/polyfill-intl-grapheme' => 'v1.23.1@16880ba9c5ebe3642d1995ab866db29270b36535',
  'symfony/polyfill-intl-normalizer' => 'v1.23.0@8590a5f561694770bdcd3f9b5c69dde6945028e8',
  'symfony/polyfill-mbstring' => 'v1.23.1@9174a3d80210dca8daa7f31fec659150bbeabfc6',
  'symfony/polyfill-php72' => 'v1.23.0@9a142215a36a3888e30d0a9eeea9766764e96976',
  'symfony/polyfill-php73' => 'v1.23.0@fba8933c384d6476ab14fb7b8526e5287ca7e010',
  'symfony/polyfill-php80' => 'v1.23.1@1100343ed1a92e3a38f9ae122fc0eb21602547be',
  'symfony/service-contracts' => 'v2.4.0@f040a30e04b57fbcc9c6cbcf4dbaa96bd318b9bb',
  'symfony/stopwatch' => 'v5.3.4@b24c6a92c6db316fee69e38c80591e080e41536c',
  'symfony/string' => 'v5.3.10@d70c35bb20bbca71fc4ab7921e3c6bda1a82a60c',
  'tracy/tracy' => 'v2.8.8@1a18df64f6b745962e9a0bdf87eccfc62309e593',
  'nette/tester' => 'v2.4.1@b54326b3c1a2c6c76d2662a06b5ad5a10d822e98',
  'phpstan/phpstan' => '1.1.2@bcea0ae85868a89d5789c75f012c93129f842934',
  'phpstan/phpstan-nette' => '1.0.0@f4654b27b107241e052755ec187a0b1964541ba6',
  'symfony/thanks' => 'v1.2.10@e9c4709560296acbd4fe9e12b8d57a925aa7eae8',
  'nette/web-project' => 'dev-master@c01da24525845298898623a0f2d932d2089b6f10',
);

    private function __construct()
    {
    }

    /**
     * @psalm-pure
     *
     * @psalm-suppress ImpureMethodCall we know that {@see InstalledVersions} interaction does not
     *                                  cause any side effects here.
     */
    public static function rootPackageName() : string
    {
        if (!self::composer2ApiUsable()) {
            return self::ROOT_PACKAGE_NAME;
        }

        return InstalledVersions::getRootPackage()['name'];
    }

    /**
     * @throws OutOfBoundsException If a version cannot be located.
     *
     * @psalm-param key-of<self::VERSIONS> $packageName
     * @psalm-pure
     *
     * @psalm-suppress ImpureMethodCall we know that {@see InstalledVersions} interaction does not
     *                                  cause any side effects here.
     */
    public static function getVersion(string $packageName): string
    {
        if (self::composer2ApiUsable()) {
            return InstalledVersions::getPrettyVersion($packageName)
                . '@' . InstalledVersions::getReference($packageName);
        }

        if (isset(self::VERSIONS[$packageName])) {
            return self::VERSIONS[$packageName];
        }

        throw new OutOfBoundsException(
            'Required package "' . $packageName . '" is not installed: check your ./vendor/composer/installed.json and/or ./composer.lock files'
        );
    }

    private static function composer2ApiUsable(): bool
    {
        if (!class_exists(InstalledVersions::class, false)) {
            return false;
        }

        if (method_exists(InstalledVersions::class, 'getAllRawData')) {
            $rawData = InstalledVersions::getAllRawData();
            if (count($rawData) === 1 && count($rawData[0]) === 0) {
                return false;
            }
        } else {
            $rawData = InstalledVersions::getRawData();
            if ($rawData === null || $rawData === []) {
                return false;
            }
        }

        return true;
    }
}
