<?php return array(
    'root' => array(
        'pretty_version' => 'dev-master',
        'version' => 'dev-master',
        'type' => 'project',
        'install_path' => __DIR__ . '/../../',
        'aliases' => array(),
        'reference' => 'c01da24525845298898623a0f2d932d2089b6f10',
        'name' => 'nette/web-project',
        'dev' => true,
    ),
    'versions' => array(
        'composer/package-versions-deprecated' => array(
            'pretty_version' => '1.11.99.4',
            'version' => '1.11.99.4',
            'type' => 'composer-plugin',
            'install_path' => __DIR__ . '/./package-versions-deprecated',
            'aliases' => array(),
            'reference' => 'b174585d1fe49ceed21928a945138948cb394600',
            'dev_requirement' => false,
        ),
        'contributte/console' => array(
            'pretty_version' => 'v0.9.1',
            'version' => '0.9.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../contributte/console',
            'aliases' => array(),
            'reference' => '549893573ba3cb81f476785763f48178b5166322',
            'dev_requirement' => false,
        ),
        'contributte/di' => array(
            'pretty_version' => 'v0.5.1',
            'version' => '0.5.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../contributte/di',
            'aliases' => array(),
            'reference' => '534fdb5e85b4ae01f8f848fc4b752deb8458ed7c',
            'dev_requirement' => false,
        ),
        'doctrine/annotations' => array(
            'pretty_version' => '1.13.2',
            'version' => '1.13.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/annotations',
            'aliases' => array(),
            'reference' => '5b668aef16090008790395c02c893b1ba13f7e08',
            'dev_requirement' => false,
        ),
        'doctrine/cache' => array(
            'pretty_version' => '1.12.1',
            'version' => '1.12.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/cache',
            'aliases' => array(),
            'reference' => '4cf401d14df219fa6f38b671f5493449151c9ad8',
            'dev_requirement' => false,
        ),
        'doctrine/collections' => array(
            'pretty_version' => '1.6.8',
            'version' => '1.6.8.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/collections',
            'aliases' => array(),
            'reference' => '1958a744696c6bb3bb0d28db2611dc11610e78af',
            'dev_requirement' => false,
        ),
        'doctrine/common' => array(
            'pretty_version' => '3.2.0',
            'version' => '3.2.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/common',
            'aliases' => array(),
            'reference' => '6d970a11479275300b5144e9373ce5feacfa9b91',
            'dev_requirement' => false,
        ),
        'doctrine/data-fixtures' => array(
            'pretty_version' => '1.5.1',
            'version' => '1.5.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/data-fixtures',
            'aliases' => array(),
            'reference' => 'f18adf13f6c81c67a88360dca359ad474523f8e3',
            'dev_requirement' => false,
        ),
        'doctrine/dbal' => array(
            'pretty_version' => '2.13.4',
            'version' => '2.13.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/dbal',
            'aliases' => array(),
            'reference' => '2411a55a2a628e6d8dd598388ab13474802c7b6e',
            'dev_requirement' => false,
        ),
        'doctrine/deprecations' => array(
            'pretty_version' => 'v0.5.3',
            'version' => '0.5.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/deprecations',
            'aliases' => array(),
            'reference' => '9504165960a1f83cc1480e2be1dd0a0478561314',
            'dev_requirement' => false,
        ),
        'doctrine/event-manager' => array(
            'pretty_version' => '1.1.1',
            'version' => '1.1.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/event-manager',
            'aliases' => array(),
            'reference' => '41370af6a30faa9dc0368c4a6814d596e81aba7f',
            'dev_requirement' => false,
        ),
        'doctrine/inflector' => array(
            'pretty_version' => '2.0.4',
            'version' => '2.0.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/inflector',
            'aliases' => array(),
            'reference' => '8b7ff3e4b7de6b2c84da85637b59fd2880ecaa89',
            'dev_requirement' => false,
        ),
        'doctrine/instantiator' => array(
            'pretty_version' => '1.4.0',
            'version' => '1.4.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/instantiator',
            'aliases' => array(),
            'reference' => 'd56bf6102915de5702778fe20f2de3b2fe570b5b',
            'dev_requirement' => false,
        ),
        'doctrine/lexer' => array(
            'pretty_version' => '1.2.1',
            'version' => '1.2.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/lexer',
            'aliases' => array(),
            'reference' => 'e864bbf5904cb8f5bb334f99209b48018522f042',
            'dev_requirement' => false,
        ),
        'doctrine/migrations' => array(
            'pretty_version' => '2.3.4',
            'version' => '2.3.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/migrations',
            'aliases' => array(),
            'reference' => '6d87c9a0baa6a4725b4c4e1a45b2a39f53bf1859',
            'dev_requirement' => false,
        ),
        'doctrine/orm' => array(
            'pretty_version' => '2.10.2',
            'version' => '2.10.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/orm',
            'aliases' => array(),
            'reference' => '81d472f6f96b8b571cafefe8d2fef89ed9446a62',
            'dev_requirement' => false,
        ),
        'doctrine/persistence' => array(
            'pretty_version' => '2.2.3',
            'version' => '2.2.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../doctrine/persistence',
            'aliases' => array(),
            'reference' => '5e7bdbbfe9811c06e1f745d1c166647d5c47d6ee',
            'dev_requirement' => false,
        ),
        'friendsofphp/proxy-manager-lts' => array(
            'pretty_version' => 'v1.0.5',
            'version' => '1.0.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../friendsofphp/proxy-manager-lts',
            'aliases' => array(),
            'reference' => '006aa5d32f887a4db4353b13b5b5095613e0611f',
            'dev_requirement' => false,
        ),
        'laminas/laminas-code' => array(
            'pretty_version' => '4.4.3',
            'version' => '4.4.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../laminas/laminas-code',
            'aliases' => array(),
            'reference' => 'bb324850d09dd437b6acb142c13e64fdc725b0e1',
            'dev_requirement' => false,
        ),
        'latte/latte' => array(
            'pretty_version' => 'v2.10.5',
            'version' => '2.10.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../latte/latte',
            'aliases' => array(),
            'reference' => '0138a26efe7a2a448769f6ba490bfaae8fe7ab50',
            'dev_requirement' => false,
        ),
        'nette/application' => array(
            'pretty_version' => 'v3.1.4',
            'version' => '3.1.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/application',
            'aliases' => array(),
            'reference' => 'db6eedc199732ed6cf9982da97a38219e8601c62',
            'dev_requirement' => false,
        ),
        'nette/bootstrap' => array(
            'pretty_version' => 'v3.1.1',
            'version' => '3.1.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/bootstrap',
            'aliases' => array(),
            'reference' => 'efe6c30fc009451f59fe56f3b309eb85c48b2baf',
            'dev_requirement' => false,
        ),
        'nette/caching' => array(
            'pretty_version' => 'v3.1.1',
            'version' => '3.1.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/caching',
            'aliases' => array(),
            'reference' => '3e771c589dee414724be473c24ad16dae50c1960',
            'dev_requirement' => false,
        ),
        'nette/component-model' => array(
            'pretty_version' => 'v3.0.2',
            'version' => '3.0.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/component-model',
            'aliases' => array(),
            'reference' => '20a39df12009029c7e425bc5e0439ee4ab5304af',
            'dev_requirement' => false,
        ),
        'nette/database' => array(
            'pretty_version' => 'v3.1.3',
            'version' => '3.1.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/database',
            'aliases' => array(),
            'reference' => '08aff4d62c80873752254b0b655027211f501c4d',
            'dev_requirement' => false,
        ),
        'nette/di' => array(
            'pretty_version' => 'v3.0.11',
            'version' => '3.0.11.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/di',
            'aliases' => array(),
            'reference' => '942e406f63b88b57cb4e095ae0fd95c103d12c5b',
            'dev_requirement' => false,
        ),
        'nette/finder' => array(
            'pretty_version' => 'v2.5.2',
            'version' => '2.5.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/finder',
            'aliases' => array(),
            'reference' => '4ad2c298eb8c687dd0e74ae84206a4186eeaed50',
            'dev_requirement' => false,
        ),
        'nette/forms' => array(
            'pretty_version' => 'v3.1.6',
            'version' => '3.1.6.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/forms',
            'aliases' => array(),
            'reference' => '4ed52434b61d7e532cb3bc77b048717703b91b0b',
            'dev_requirement' => false,
        ),
        'nette/http' => array(
            'pretty_version' => 'v3.1.3',
            'version' => '3.1.3.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/http',
            'aliases' => array(),
            'reference' => '999f54884deed7a419b13552271c7059a5acb428',
            'dev_requirement' => false,
        ),
        'nette/mail' => array(
            'pretty_version' => 'v3.1.7',
            'version' => '3.1.7.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/mail',
            'aliases' => array(),
            'reference' => '0825a071b896c2ed8cbe1af776c2539acd7bb976',
            'dev_requirement' => false,
        ),
        'nette/neon' => array(
            'pretty_version' => 'v3.3.1',
            'version' => '3.3.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/neon',
            'aliases' => array(),
            'reference' => '1f4e5f6a30bf45b6c2c932be7396ea70692ee607',
            'dev_requirement' => false,
        ),
        'nette/php-generator' => array(
            'pretty_version' => 'v3.6.4',
            'version' => '3.6.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/php-generator',
            'aliases' => array(),
            'reference' => 'b8375ac20760c62b6816f8c2eaeabbbca305eed7',
            'dev_requirement' => false,
        ),
        'nette/robot-loader' => array(
            'pretty_version' => 'v3.4.1',
            'version' => '3.4.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/robot-loader',
            'aliases' => array(),
            'reference' => 'e2adc334cb958164c050f485d99c44c430f51fe2',
            'dev_requirement' => false,
        ),
        'nette/routing' => array(
            'pretty_version' => 'v3.0.2',
            'version' => '3.0.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/routing',
            'aliases' => array(),
            'reference' => '5532e7e3612e13def357f089c1a5c25793a16843',
            'dev_requirement' => false,
        ),
        'nette/schema' => array(
            'pretty_version' => 'v1.2.2',
            'version' => '1.2.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/schema',
            'aliases' => array(),
            'reference' => '9a39cef03a5b34c7de64f551538cbba05c2be5df',
            'dev_requirement' => false,
        ),
        'nette/security' => array(
            'pretty_version' => 'v3.1.5',
            'version' => '3.1.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/security',
            'aliases' => array(),
            'reference' => 'c120893f561b09494486c66594720b2abcb099b2',
            'dev_requirement' => false,
        ),
        'nette/tester' => array(
            'pretty_version' => 'v2.4.1',
            'version' => '2.4.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/tester',
            'aliases' => array(),
            'reference' => 'b54326b3c1a2c6c76d2662a06b5ad5a10d822e98',
            'dev_requirement' => true,
        ),
        'nette/utils' => array(
            'pretty_version' => 'v3.2.5',
            'version' => '3.2.5.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nette/utils',
            'aliases' => array(),
            'reference' => '9cd80396ca58d7969ab44fc7afcf03624dfa526e',
            'dev_requirement' => false,
        ),
        'nette/web-project' => array(
            'pretty_version' => 'dev-master',
            'version' => 'dev-master',
            'type' => 'project',
            'install_path' => __DIR__ . '/../../',
            'aliases' => array(),
            'reference' => 'c01da24525845298898623a0f2d932d2089b6f10',
            'dev_requirement' => false,
        ),
        'nettrine/annotations' => array(
            'pretty_version' => 'v0.7.0',
            'version' => '0.7.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nettrine/annotations',
            'aliases' => array(),
            'reference' => 'fbb06d156a4edcbf37e4154e5b4ede079136388b',
            'dev_requirement' => false,
        ),
        'nettrine/cache' => array(
            'pretty_version' => 'v0.3.0',
            'version' => '0.3.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nettrine/cache',
            'aliases' => array(),
            'reference' => '8a58596de24cdd61e45866ef8f35788675f6d2bc',
            'dev_requirement' => false,
        ),
        'nettrine/dbal' => array(
            'pretty_version' => 'v0.7.0',
            'version' => '0.7.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nettrine/dbal',
            'aliases' => array(),
            'reference' => '26a63ef21d229aa35b42b2d7798581bdb88ee912',
            'dev_requirement' => false,
        ),
        'nettrine/fixtures' => array(
            'pretty_version' => 'v0.6.0',
            'version' => '0.6.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nettrine/fixtures',
            'aliases' => array(),
            'reference' => '395476f575f48cd37d66f7856b0804b9534455f8',
            'dev_requirement' => false,
        ),
        'nettrine/migrations' => array(
            'pretty_version' => 'v0.7.1',
            'version' => '0.7.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nettrine/migrations',
            'aliases' => array(),
            'reference' => '71951f68fec491e484e628047a3a2fe21469a518',
            'dev_requirement' => false,
        ),
        'nettrine/orm' => array(
            'pretty_version' => 'v0.8.2',
            'version' => '0.8.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../nettrine/orm',
            'aliases' => array(),
            'reference' => 'fc84bccce8665255519266351bb6b746d1d58650',
            'dev_requirement' => false,
        ),
        'ocramius/package-versions' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '1.11.99',
            ),
        ),
        'ocramius/proxy-manager' => array(
            'dev_requirement' => false,
            'replaced' => array(
                0 => '^2.1',
            ),
        ),
        'phpstan/phpstan' => array(
            'pretty_version' => '1.1.2',
            'version' => '1.1.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../phpstan/phpstan',
            'aliases' => array(),
            'reference' => 'bcea0ae85868a89d5789c75f012c93129f842934',
            'dev_requirement' => true,
        ),
        'phpstan/phpstan-nette' => array(
            'pretty_version' => '1.0.0',
            'version' => '1.0.0.0',
            'type' => 'phpstan-extension',
            'install_path' => __DIR__ . '/../phpstan/phpstan-nette',
            'aliases' => array(),
            'reference' => 'f4654b27b107241e052755ec187a0b1964541ba6',
            'dev_requirement' => true,
        ),
        'psr/cache' => array(
            'pretty_version' => '3.0.0',
            'version' => '3.0.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/cache',
            'aliases' => array(),
            'reference' => 'aa5030cfa5405eccfdcb1083ce040c2cb8d253bf',
            'dev_requirement' => false,
        ),
        'psr/container' => array(
            'pretty_version' => '1.1.2',
            'version' => '1.1.2.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../psr/container',
            'aliases' => array(),
            'reference' => '513e0666f7216c7459170d56df27dfcefe1689ea',
            'dev_requirement' => false,
        ),
        'psr/log-implementation' => array(
            'dev_requirement' => false,
            'provided' => array(
                0 => '1.0|2.0',
            ),
        ),
        'symfony/console' => array(
            'pretty_version' => 'v5.3.10',
            'version' => '5.3.10.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/console',
            'aliases' => array(),
            'reference' => 'd4e409d9fbcfbf71af0e5a940abb7b0b4bad0bd3',
            'dev_requirement' => false,
        ),
        'symfony/deprecation-contracts' => array(
            'pretty_version' => 'v2.4.0',
            'version' => '2.4.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/deprecation-contracts',
            'aliases' => array(),
            'reference' => '5f38c8804a9e97d23e0c8d63341088cd8a22d627',
            'dev_requirement' => false,
        ),
        'symfony/filesystem' => array(
            'pretty_version' => 'v5.3.4',
            'version' => '5.3.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/filesystem',
            'aliases' => array(),
            'reference' => '343f4fe324383ca46792cae728a3b6e2f708fb32',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-ctype' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-ctype',
            'aliases' => array(),
            'reference' => '46cd95797e9df938fdd2b03693b5fca5e64b01ce',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-grapheme' => array(
            'pretty_version' => 'v1.23.1',
            'version' => '1.23.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-grapheme',
            'aliases' => array(),
            'reference' => '16880ba9c5ebe3642d1995ab866db29270b36535',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-intl-normalizer' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-intl-normalizer',
            'aliases' => array(),
            'reference' => '8590a5f561694770bdcd3f9b5c69dde6945028e8',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-mbstring' => array(
            'pretty_version' => 'v1.23.1',
            'version' => '1.23.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-mbstring',
            'aliases' => array(),
            'reference' => '9174a3d80210dca8daa7f31fec659150bbeabfc6',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php72' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php72',
            'aliases' => array(),
            'reference' => '9a142215a36a3888e30d0a9eeea9766764e96976',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php73' => array(
            'pretty_version' => 'v1.23.0',
            'version' => '1.23.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php73',
            'aliases' => array(),
            'reference' => 'fba8933c384d6476ab14fb7b8526e5287ca7e010',
            'dev_requirement' => false,
        ),
        'symfony/polyfill-php80' => array(
            'pretty_version' => 'v1.23.1',
            'version' => '1.23.1.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/polyfill-php80',
            'aliases' => array(),
            'reference' => '1100343ed1a92e3a38f9ae122fc0eb21602547be',
            'dev_requirement' => false,
        ),
        'symfony/service-contracts' => array(
            'pretty_version' => 'v2.4.0',
            'version' => '2.4.0.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/service-contracts',
            'aliases' => array(),
            'reference' => 'f040a30e04b57fbcc9c6cbcf4dbaa96bd318b9bb',
            'dev_requirement' => false,
        ),
        'symfony/stopwatch' => array(
            'pretty_version' => 'v5.3.4',
            'version' => '5.3.4.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/stopwatch',
            'aliases' => array(),
            'reference' => 'b24c6a92c6db316fee69e38c80591e080e41536c',
            'dev_requirement' => false,
        ),
        'symfony/string' => array(
            'pretty_version' => 'v5.3.10',
            'version' => '5.3.10.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../symfony/string',
            'aliases' => array(),
            'reference' => 'd70c35bb20bbca71fc4ab7921e3c6bda1a82a60c',
            'dev_requirement' => false,
        ),
        'symfony/thanks' => array(
            'pretty_version' => 'v1.2.10',
            'version' => '1.2.10.0',
            'type' => 'composer-plugin',
            'install_path' => __DIR__ . '/../symfony/thanks',
            'aliases' => array(),
            'reference' => 'e9c4709560296acbd4fe9e12b8d57a925aa7eae8',
            'dev_requirement' => true,
        ),
        'tracy/tracy' => array(
            'pretty_version' => 'v2.8.8',
            'version' => '2.8.8.0',
            'type' => 'library',
            'install_path' => __DIR__ . '/../tracy/tracy',
            'aliases' => array(),
            'reference' => '1a18df64f6b745962e9a0bdf87eccfc62309e593',
            'dev_requirement' => false,
        ),
    ),
);
